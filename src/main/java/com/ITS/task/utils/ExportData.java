package com.ITS.task.utils;

import com.ITS.task.entity.Employee;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import java.util.List;
import java.util.Map;

public class ExportData extends AbstractXlsView {
    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.addHeader("Content-Disposition", "attachment;fileName=Employee.xls");

        // read data provided by controller
        @SuppressWarnings("unchecked")
        List<Employee> employeeList = (List<Employee>) model.get("list");

        // create one sheet
        Sheet sheet = workbook.createSheet("Employee");

        // create row0 as a header
        Row row0 = sheet.createRow(0);
        row0.createCell(0).setCellValue("employeeNumber");
        row0.createCell(1).setCellValue("name");
        row0.createCell(2).setCellValue("phone");
        row0.createCell(3).setCellValue("position");
        row0.createCell(3).setCellValue("email");

        // create row1 onwards from List<T>
        int rowNum = 1;
        for(Employee employee : employeeList) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(employee.getEmployeeNumber());
            row.createCell(1).setCellValue(employee.getName());
            row.createCell(2).setCellValue(employee.getPhone());
            row.createCell(3).setCellValue(employee.getPosition());
            row.createCell(3).setCellValue(employee.getEmail());
        }
    }
}
