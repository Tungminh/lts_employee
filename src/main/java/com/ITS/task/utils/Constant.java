package com.ITS.task.utils;

public class Constant {
    public static class REGEX {
        public static final String EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        public static final String PHONE = "(0[3|5|7|8|9])+([0-9]{8})";
    }
}
