package com.ITS.task.entity;

import jakarta.validation.constraints.Size;

public class Employee {
    private Integer employeeNumber;

    private String name;

    @Size(max = 10, min = 10, message = "Mobile number should be of 10 digits")
    private String phone;

    private String email;

    private String position;

    public Employee() {
    }


    public Employee(Integer employeeNumber, String name, String phone, String email, String position) {
        this.employeeNumber = employeeNumber;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.position = position;
    }

    public Integer getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(Integer employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
