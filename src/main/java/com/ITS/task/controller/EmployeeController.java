package com.ITS.task.controller;

import com.ITS.task.entity.Employee;
import com.ITS.task.service.EmployeeService;
import com.ITS.task.utils.ExportData;
import jakarta.mail.MessagingException;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;


    @PostMapping("/save")
    public String save(@Valid @ModelAttribute("employee") Employee employee, Errors errors) {
        if (null != errors && errors.getErrorCount() > 0) {
            return "register";
        } else {
            String result = employeeService.save(employee);
            return "redirect:/getAll";
        }
    }

    @PostMapping("/edit")
    public String update(@Valid @ModelAttribute("employee") Employee employee, Errors errors) {
        if (null != errors && errors.getErrorCount() > 0) {
            return "register";
        } else {
            String result = employeeService.update(employee);
            return "redirect:/getAll";
        }
    }

    @GetMapping("/getByEmployeeNumber/{employeeNumber}")
    public Employee getByEmployeeNumber(@PathVariable("employeeNumber")Long employeeNumber) {
        Employee employee = employeeService.getByEmployeeNumber(employeeNumber);
        return employee;
    }

    @GetMapping("/getAll")
    public String getAll(Model model) {
        List<Employee> employeeList = employeeService.getAll();
        model.addAttribute("employeeList", employeeList);
        return "index";
    }

    @GetMapping("/find")
    public List<Employee> find(@RequestParam(required=false, name = "name") String name,
                               @RequestParam(required=false, name = "phone") String phone,
                               @RequestParam(required=false, name = "email") String email,
                               @RequestParam(required=false, name = "position") String position) {
        List<Employee> employeeList = employeeService.get(name, phone, email, position);
        return employeeList;
    }
    @GetMapping("/delete/{employeeNumber}")
    public String delete(@PathVariable("employeeNumber")Long employeeNumber) {
        this.employeeService.delete(employeeNumber);
        return "redirect:/getAll";
    }

    @GetMapping("/add")
    public String showNewEmployeeForm(Model model) {
        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        return "register";
    }

    @GetMapping("/update/{employeeNumber}")
    public String showFormForUpdate(@PathVariable( value = "employeeNumber") long employeeNumber, Model model) {

        Employee employee = employeeService.getByEmployeeNumber(employeeNumber);
        model.addAttribute("employee", employee);
        return "update";
    }

    @GetMapping("/excelExport")
    public ModelAndView exportToExcel() {
        ModelAndView mav = new ModelAndView();
        mav.setView(new ExportData());
        //read data from DB
        List<Employee> list= employeeService.getAll();
        //send to excelImpl class
        mav.addObject("list", list);
        return mav;
    }


    @Autowired
    private JavaMailSender mailSender;

    @GetMapping("")
    public String viewHomePage() {
        return "index";
    }
    @GetMapping("/send_html_email")
    public String sendHTMLEmail() throws MessagingException {
        this.employeeService.sendEmail();
        return "result";
    }
}
