package com.ITS.task.service.impl;

import com.ITS.task.entity.Employee;
import com.ITS.task.repository.EmployeeRepository;
import com.ITS.task.service.EmployeeService;
import com.ITS.task.utils.Validate;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private Validate validate;

    @Override
    public String update(Employee employee) {
//        if (!validate.checkValidateEmail(employee.getEmail())) return "email not right format,try again!";
//        if (!validate.checkValidatePhone(employee.getPhone()))
//            return "phone not right format, Try again!";
        employeeRepository.update(employee);
        return "successfully";
    }

    @Override
    public String save(Employee employee) {
//        if (!validate.checkValidateEmail(employee.getEmail())) return "email not right format,try again!";
//        if (!validate.checkValidatePhone(employee.getPhone()))
//            return "phone not right format, Try again!";
        employeeRepository.register(employee);
        return "successfully";
    }

    @Override
    public List<Employee> getAll() {
        List<Employee> employeeList = new ArrayList<>();
        employeeList = employeeRepository.findAll();
        return employeeList;
    }

    @Override
    public String delete(Long employeeNumber) {
        employeeRepository.delete(employeeNumber);
        return "successfully";
    }

    @Override
    public Employee getByEmployeeNumber(Long employeeNumber) {
        Employee employee = employeeRepository.findByEmployeeNumber(employeeNumber);
        return employee;
    }

    @Override
    public List<Employee> get(String name, String phone, String email, String position) {
        List<Employee> employeeList1 = new ArrayList<>();
        employeeList1 = employeeRepository.find(name, phone, email, position);
        return employeeList1;
    }

    @Autowired
    private JavaMailSender mailSender;

//    @Scheduled(cron = "0 */2 * ? * *")
    public void sendEmail() throws MessagingException {
        String from = "tunglm.iist@gmail.com";
        String to = "tungminhle.mt@gmail.com";

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setSubject("This is an HTML email");
        helper.setFrom(from);
        helper.setTo(to);

        boolean html = true;
        helper.setText("<b>Hey guys</b>,<br><i>Welcome to my new home</i>", html);
        String content = "<h2>list Employee</h2>";
        List<Employee> employeeList = employeeRepository.findAll();
        for (Employee employee : employeeList) {
            content += "<p>" +employee.getEmployeeNumber()+" "+employee.getName()+" "+employee.getPhone()+" "+employee.getEmail()+" "+employee.getPosition()+"<p>";
        }

        helper.setText(content, true);

        mailSender.send(message);
    }
}
