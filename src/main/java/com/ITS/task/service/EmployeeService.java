package com.ITS.task.service;

import com.ITS.task.entity.Employee;
import jakarta.mail.MessagingException;
import org.springframework.ui.Model;

import java.util.List;

public interface EmployeeService {
    String update(Employee employee);

    String save(Employee employee);

    List<Employee> getAll();

    String delete(Long employeeNumber);

    Employee getByEmployeeNumber(Long employeeNumber);

    List<Employee> get(String name, String phone, String email, String position);

    void sendEmail(Model model) throws MessagingException;
}
