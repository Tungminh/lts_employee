package com.ITS.task.repository;

import com.ITS.task.entity.Employee;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface EmployeeRepository {
    @Select("select * from employee order by name asc")
    public List<Employee> findAll();

    @Select("SELECT * FROM employee WHERE name like #{name} and phone like #{phone} and email like #{email} and position like #{position}")
    public List<Employee> find(String name, String phone, String email, String position);

    @Select("SELECT * FROM employee WHERE employeeNumber= #{employeeNumber}")
    public Employee findByEmployeeNumber(long employeeNumber);

    @Delete("DELETE FROM employee WHERE employeeNumber = #{employeeNumber}")
    public int delete(long employeeNumber);

    @Insert("INSERT INTO employee(employeeNumber, name, phone, position, email) " +
            " VALUES (#{employeeNumber}, #{name}, #{phone}, #{position}, #{email})")
    public int register(Employee employee);

    @Update("Update employee set name=#{name}, " +
            " phone=#{phone}, position=#{position}, email=#{email} where employeeNumber=#{employeeNumber}")
    public int update(Employee employee);
}
